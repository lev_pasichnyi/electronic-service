const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const postRoute = require('./routes/posts');
const authRoute = require('./routes/auth');

dotenv.config();

// Authorization to MongoDB, pending connection
mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true });

// check database connection, opening
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('We are connected!');
});

//Middleware
app.use(express.json());

// Route middleware
app.use('/api/client', authRoute);
app.use('/api/posts', postRoute);

app.listen(3000, () => console.log('Server up and running'));