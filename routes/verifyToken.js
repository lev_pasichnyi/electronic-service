const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    // if auth-token present in html header, then verify
    const token = req.header('auth-token');
    if (!token) return res.status(401).send('Access denied');

    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET);
        req.client = verified;
        next();
    } catch (error) {
        res.status(400).send('Invalid token');
    }
}
