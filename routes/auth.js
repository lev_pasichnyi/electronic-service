const router = require('express').Router();
const Client = require('../model/Client');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { registerValidation, loginValidation } = require('../validation');

// create a new, unique user (check in DB if email present)
router.post('/register', async (req, res) => {

    // validation of input data
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // check if the Client is already in the database
    const emailExist = await Client.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('email already in the DB');

    // hash input passwords with salt
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    // create a new client from input with hashed password
    const client = new Client({
        name: req.body.name,
        surname: req.body.surname,
        firstPhone: req.body.firstPhone,
        secondPhone: req.body.secondPhone,
        email: req.body.email,
        character: req.body.character,
        password: hashedPassword
    });
    // save to MongoDB and return client id in response 
    try {
        const savedClient = await client.save();
        res.send({ client: client._id });
    } catch (error) {
        res.status(400).send(error);
    }
});

// login form validation
router.post('/login', async (req, res) => {
    // validate data before we a client
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    // check if user in DB
    const client = await Client.findOne({ email: req.body.email });
    if (!client) return res.status(400).send('Email is wrong');
    // password is correct
    const validPass = await bcrypt.compare(req.body.password, client.password);
    if (!validPass) return res.status(400).send('Password is not match')

    // create and assign a token
    const token = jwt.sign({ _id: client._id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);
});

module.exports = router;