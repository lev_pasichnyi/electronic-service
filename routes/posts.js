const router = require('express').Router();
const verify = require('./verifyToken');

// if token verified, return client id and iat from token
router.get('/', verify, (req, res) => {
    res.send(req.client);
});

module.exports = router;