const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 3,
        max: 255
    },
    surname: {
        type: String,
        required: false,
        min: 3,
        max: 255
    },
    firstPhone: {
        type: String,
        required: false,
        min: 5,
        max: 20
    },
    secondPhone: {
        type: String,
        required: false,
        min: 5,
        max: 20
    },
    email: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    character: {
        type: String,
        required: false,
        max: 1024,
    },
    data: {
        type: Date,
        default: Date.now
    },
    password: {
        type: String,
        required: true,
        max: 1024,
        min: 6
    }
});

module.exports = mongoose.model('Client', clientSchema);